import random
AHORCADO=['''
     +---+
      |   |
          |
          |
          |
          |
    =========''', '''
      +---+
      |   |
      O   |
          |
          |
          |
    =========''', '''
      +---+
      |   |
      O   |
      |   |
          |
          |
    =========''', '''
      +---+
      |   |
      O   |
     /|   |
          |
          |
    =========''', '''
      +---+
      |   |
      O   |
     /|\  |
          |
          |
    =========''', '''
      +---+
      |   |
      O   |
     /|\  |
     /    |
          |
    =========''', '''
      +---+
      |   |
      O   |
     /|\  |
     / \  |
          |
    =========''']
palabrassecretas='imposible python carta comida juego paulocoronado programacionorientadaobjetos ayuda amor independiente concierto ropa calculo suelos fotogrametria catedra casa monitor computador mapaconceptual cantante volumen lapiz celular anillos muñeco corazon'.split()
#Funcion que retorna una palabra secreta aleatoria

def Palabra_Aleatoria(lista_Palabras):
      PalabrasecretaAleatoria=random.randint(0, len(lista_Palabras) - 1)
      return lista_Palabras[PalabrasecretaAleatoria]

def Dibujo_e_Incorrecta(AHORCADO,letra_incorrecta,letra_correcta,palabra_escogida):
#Funcion que muestra el avance del ahorcado y verifica si la letra ingresada es incorrecta
      print(AHORCADO[len(letra_incorrecta)])
      print("")
      fin= " "
      print('Las letras que no contiene la palabra secreta son:',fin)
      for letras in letra_incorrecta:
            print(letras, fin)
      print("")
      espacios_blanco = '_' * len(palabra_escogida)
      for i in range(len(palabra_escogida)):
            if palabra_escogida[i] in letra_correcta:
                  espacios_blanco=espacios_blanco[:i]+palabra_escogida[i]+espacios_blanco[i+1]
      for letra in espacios_blanco:
            print (letras,fin)
      print("")

